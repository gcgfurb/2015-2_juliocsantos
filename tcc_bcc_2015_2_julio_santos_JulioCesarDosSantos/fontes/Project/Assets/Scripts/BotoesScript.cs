﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BotoesScript : MonoBehaviour
{
	public void VoltarTelaInicial ()
	{
		Application.LoadLevel ("TelaInicial");
	}

	public void Sobre ()
	{
		Application.OpenURL ("http://www.inf.furb.br/gcg/tecedu/transito");	
	}

	public void EducacaoNoTransito ()
	{
		Application.OpenURL ("http://www.inf.furb.br/gcg/tecedu/transito/educa.pdf");
	}

	public void Imprimir ()
	{
		Application.OpenURL ("http://www.inf.furb.br/gcg/tecedu/transito/mapa.pdf");
	}

	public void Iniciar ()
	{
		Application.LoadLevel ("ControlePorJoystick");
	}
}
