﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class MarcadorMapaScript : MonoBehaviour, ITrackableEventHandler
{
	private TrackableBehaviour mTrackableBehaviour = null;
	
	// Use this for initialization
	void Start ()
	{
		this.mTrackableBehaviour = GetComponent<TrackableBehaviour> ();
		if (mTrackableBehaviour) {
			this.mTrackableBehaviour.RegisterTrackableEventHandler (this);
		}
	}
	
	public void OnTrackableStateChanged (
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {	
			OnTrackingFound ();
			GameObject joystick = GameObject.FindGameObjectWithTag ("MobJoystick");
			GameObject joy = GameObject.FindGameObjectWithTag ("JoystickControl");
			if (joy != null && joy != null && joy.transform.childCount == 0) {
				joystick.transform.parent = joy.transform;
			}
		} else {	
		
			GameObject joy = GameObject.FindGameObjectWithTag ("JoystickControl");
			if (joy != null && joy.transform.childCount > 0) {
				joy.transform.DetachChildren ();
			}
			
			OnTrackingLost ();
		}
	}
	
	private void OnTrackingFound ()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer> (true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider> (true);
		
		// Enable rendering:
		foreach (Renderer component in rendererComponents) {
			component.enabled = true;
		}
		
		// Enable colliders:
		foreach (Collider component in colliderComponents) {
			component.enabled = true;
		}
	}
	
	private void OnTrackingLost ()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer> (true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider> (true);
		
		// Disable rendering:
		foreach (Renderer component in rendererComponents) {
			component.enabled = false;
		}
		
		// Disable colliders:
		foreach (Collider component in colliderComponents) {
			component.enabled = false;
		}
	}
}
