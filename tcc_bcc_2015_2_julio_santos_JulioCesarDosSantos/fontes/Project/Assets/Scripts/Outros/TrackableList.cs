﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vuforia;
using UnityEngine.UI;

public class TrackableList : MonoBehaviour {

	private ComparadorPosicao3D ComparadorPos;
	private ControleBarrasScript ControleGasolina;
	// Use this for initialization


	void Start () 
	{
		this.ComparadorPos = new ComparadorPosicao3D();
		this.ControleGasolina = FindObjectOfType<ControleBarrasScript> ();
	}
	// Update is called once per frame
	void Update () 
	{
		//GameObject.FindGameObjectWithTag("FPS").GetComponent<Text> ().text = ((int)(1.0f / Time.smoothDeltaTime)).ToString();

		StateManager sm = TrackerManager.Instance.GetStateManager ();
		IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours ();
		bool mapa = false;
		bool carro = false;
		foreach (TrackableBehaviour item in activeTrackables) 
		{
			if(item.name == "ImgTargetMapaGrande" || item.name == "ImgTargetMapaPequeno")
			{
				ComparadorPos.ObjetoUm = Camera.main.WorldToScreenPoint(item.transform.position);
				mapa = true;
			}
			if(item.name.IndexOf(CarroConfig.Instancia.tagCarroDoMarcadorSelecionado) != -1)
			{
				ComparadorPos.ObjetoDois = Camera.main.WorldToScreenPoint(item.transform.position);
				carro = true;
			}
		}

		if (mapa && carro) 
		{
			if (ComparadorPos.PosicoesDiferentes ()) 
			{
				this.ControleGasolina.DiminuiGasolinaAtual (1);
			}
			if(this.ControleGasolina.gasolinaAtual == 0)
			{
				foreach (GameObject item in GameObject.FindGameObjectsWithTag(CarroConfig.Instancia.tagCarroDoMarcadorSelecionado)) {
					Destroy(item);
				}
			}
		} 
		else 
		{
			this.ComparadorPos.DiferencaAnteriorEntreOsObjetos = Vector3.zero;
		}
	}

	private class ComparadorPosicao3D{
		
		public Vector3 ObjetoUm;
		public Vector3 ObjetoDois;
		public Vector3 DiferencaAnteriorEntreOsObjetos;

		public ComparadorPosicao3D()
		{
			this.ObjetoUm = Vector3.zero;
			this.ObjetoUm = Vector3.zero;
			this.ObjetoDois = Vector3.zero;
			this.DiferencaAnteriorEntreOsObjetos = Vector3.zero;
		}

		public bool PosicoesDiferentes()
		{
			bool posicoesDiferentes = false;

			if (this.DiferencaAnteriorEntreOsObjetos == Vector3.zero && this.ObjetoUm != Vector3.zero && this.ObjetoDois != Vector3.zero)
			{
				this.DiferencaAnteriorEntreOsObjetos.x = this.ObjetoUm.x - this.ObjetoDois.x;
				this.DiferencaAnteriorEntreOsObjetos.y = this.ObjetoUm.y - this.ObjetoDois.y;
				this.DiferencaAnteriorEntreOsObjetos.z = this.ObjetoUm.z - this.ObjetoDois.z; 
			}

			Vector3 DifObjetos = new Vector3 (
				Mathf.Round ((this.ObjetoUm.x - this.ObjetoDois.x) - this.DiferencaAnteriorEntreOsObjetos.x),
				Mathf.Round ((this.ObjetoUm.y - this.ObjetoDois.y) - this.DiferencaAnteriorEntreOsObjetos.y),
				Mathf.Round ((this.ObjetoUm.z - this.ObjetoDois.z) - this.DiferencaAnteriorEntreOsObjetos.z));


			GameObject.FindGameObjectWithTag ("BotaoVoltarX").transform.FindChild ("Text").GetComponent<Text> ().text = DifObjetos.x.ToString();
			GameObject.FindGameObjectWithTag ("BotaoVoltarY").transform.FindChild ("Text").GetComponent<Text> ().text = DifObjetos.y.ToString();
			GameObject.FindGameObjectWithTag ("BotaoVoltarZ").transform.FindChild ("Text").GetComponent<Text> ().text = DifObjetos.z.ToString();


			if (((DifObjetos.x != 0  && (DifObjetos.x >= 10 || DifObjetos.x <= -10)) ||
			     (DifObjetos.y != 0  && (DifObjetos.y >= 9 || DifObjetos.y <= -9))) && 
			    (DifObjetos.z == 1 || DifObjetos.z == 0 || DifObjetos.z == -1)){
				posicoesDiferentes =  true;
			}

			this.DiferencaAnteriorEntreOsObjetos.x = this.ObjetoUm.x - this.ObjetoDois.x;
			this.DiferencaAnteriorEntreOsObjetos.y = this.ObjetoUm.y - this.ObjetoDois.y;
			this.DiferencaAnteriorEntreOsObjetos.z = this.ObjetoUm.z - this.ObjetoDois.z;
			return posicoesDiferentes;
		}
	}
}
