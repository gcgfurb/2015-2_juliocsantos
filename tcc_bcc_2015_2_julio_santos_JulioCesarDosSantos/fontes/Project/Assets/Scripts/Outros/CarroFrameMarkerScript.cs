﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.UI;

public class CarroFrameMarkerScript : MonoBehaviour, ITrackableEventHandler {
	
	private TrackableBehaviour mTrackableBehaviour;
	
	// Use this for initialization
	void Start () 
	{
		this.mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			this.mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	void Update(){
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			OnTrackingFound();
		}
		else
		{
			OnTrackingLost();
		}
	}
	
	private void OnTrackingFound()
	{
		
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
		
		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = true;
		}
	}

	private void OnTrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		
		// Disable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		
		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = false;
		}

		Transform Carro = gameObject.transform.FindChild ("Carro Vermelho");
		if (Carro != null)
		{
			Carro.FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 0);
			if(CarroConfig.Instancia.emCollisionStayComOutroCarro)
			{
				GameObject.FindGameObjectWithTag(CarroConfig.Instancia.tagCarroAdversario).transform.FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 0);
				CarroConfig.Instancia.emCollisionStayComOutroCarro = false;
			}
		} 
		else
		{
			gameObject.transform.FindChild ("Carro Amarelo").FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 0);
			if(CarroConfig.Instancia.emCollisionStayComOutroCarro)
			{
				GameObject.FindGameObjectWithTag(CarroConfig.Instancia.tagCarroAdversario).transform.FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 0);
				CarroConfig.Instancia.emCollisionStayComOutroCarro = false;
			}
		}
	}
}
