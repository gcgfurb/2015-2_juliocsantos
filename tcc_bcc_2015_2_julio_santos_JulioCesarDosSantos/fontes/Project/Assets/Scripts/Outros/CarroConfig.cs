﻿using UnityEngine;
using System.Collections;

public class CarroConfig {

	private CarroConfig() {}

	private static volatile CarroConfig InstanciaCarroConfig;

	public static CarroConfig Instancia
	{
		get 
		{
			if (InstanciaCarroConfig == null) 
			{
				lock (typeof(CarroConfig)) 
				{
					InstanciaCarroConfig = new CarroConfig ();
				}
			}
			return InstanciaCarroConfig;
		}
	}

	public void Limpar()
	{
		InstanciaCarroConfig = null;
	}
	
	public bool selecionouMarcadorMapa { get; set;}
	public bool emCollisionStayComOutroCarro { get; set;}
	public string nomeCarroDoMarcadorSelecionado { get; set;}
	public string tagCarroDoMarcadorSelecionado { get; set;}
	public string tagCarroAdversario { get; set;}
}
