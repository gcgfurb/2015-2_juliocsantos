﻿using UnityEngine;
using System.Collections;

public class CarroScript : MonoBehaviour {

	private ControleBarrasScript Barras;
	private bool aguardarArrumarCarro = false;
	private bool aguardarEncherTanque = false;
	private bool aguardarColisao = false;

	// Use this for initialization
	void Start () 
	{
		this.Barras = FindObjectOfType<ControleBarrasScript>();
	}

	void OnTriggerEnter(Collider Colider) 
	{
		if (gameObject.tag == CarroConfig.Instancia.tagCarroDoMarcadorSelecionado) 
		{ 
			string objetoColidido = Colider.gameObject.tag;
			switch (objetoColidido) 
			{
				case "Mecanica":
				case "PostoGasolina":
				case "Casa":
					this.Barras.DiminuiMecanicaAtual (2);
					break;
				case "CarroVermelho":
				case "CarroAmarelo":
					this.Barras.DiminuiMecanicaAtual (3);
					break;
				case "Buraco":
					this.Barras.DiminuiMecanicaAtual (1);
					break;
			}
			if (Barras.mecanicaAtual <= 0) 
			{
				Destroy (gameObject);
				Destroy (GameObject.FindGameObjectWithTag (CarroConfig.Instancia.tagCarroDoMarcadorSelecionado));
			}
		}
	}

	IEnumerator DiminuirMecanicaQuandoEntrarNoObjeto()
	{
		this.Barras.DiminuiMecanicaAtual (1);
		this.aguardarColisao = true;
		yield return new WaitForSeconds(1);
		this.aguardarColisao = false;
	}

	IEnumerator EncherTanqueGasolina()
	{
		this.Barras.IncrementaGasolinaAtual (20);
		this.aguardarEncherTanque = true;
		yield return new WaitForSeconds(2);
		this.aguardarEncherTanque = false;
	}

	IEnumerator ArrumarCarro()
	{
		this.Barras.IncrementaMecanicaAtual (6);
		this.aguardarArrumarCarro = true;
		yield return new WaitForSeconds(2);
		this.aguardarArrumarCarro = false;
	}
	
	void OnTriggerStay(Collider Colider){
		string objetoColidido = Colider.gameObject.tag;
		if (objetoColidido != "Gasolina" && objetoColidido != "Mecanica" && objetoColidido != "Esfera") 
		{
			gameObject.transform.FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 255);
		}
		if (gameObject.tag == CarroConfig.Instancia.tagCarroDoMarcadorSelecionado) 
		{ 
			switch (objetoColidido) 
			{
				case "CarroVermelho":
				case "CarroAmarelo":
				case "Casa":
				case "Mecanica":
				case "PostoGasolina":
					if (!this.aguardarColisao)
					{
						StartCoroutine (DiminuirMecanicaQuandoEntrarNoObjeto ());
					}
					if(Barras.mecanicaAtual <= 0)
					{
						Colider.gameObject.transform.FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 0);
					}
				CarroConfig.Instancia.emCollisionStayComOutroCarro = true;
					break;
				case "ChaveInglesa":
					if (!this.aguardarArrumarCarro) 
					{
						StartCoroutine (ArrumarCarro ());
					}
					break;
				case "GalaoDeGasolina":
					if (!this.aguardarEncherTanque) 
					{
						StartCoroutine (EncherTanqueGasolina ());
					}
					break;
				}

				if (Barras.mecanicaAtual <= 0) 
				{
					Destroy (gameObject);
					Destroy (GameObject.FindGameObjectWithTag (CarroConfig.Instancia.tagCarroDoMarcadorSelecionado));
				}	    
		}
	}

	void OnTriggerExit(Collider Colider) 
	{
		gameObject.transform.FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 0);
		Colider.transform.FindChild ("negativo").GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255, 0);
		if (Colider.tag == CarroConfig.Instancia.tagCarroAdversario) 
		{
			CarroConfig.Instancia.emCollisionStayComOutroCarro = false;
		}
	}

	// Update is called once per frame
	void Update ()
	{	
	}
}
