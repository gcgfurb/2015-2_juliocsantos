﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class xBotoesScript : MonoBehaviour {

	private IEnumerator Agurdar(int segundos) {
		yield return new WaitForSeconds(segundos);
		GameObject.FindGameObjectWithTag ("TextoMarcadorSelecao").GetComponent<Text> ().text = string.Empty;
	}

	public void VoltarTelaInicial(){
		Application.LoadLevel ("TelaInicial");
	}

	public void EducacaoNoTransito(){
		Application.OpenURL("http://gcg.inf.furb.br/visedu/life/visedutransito/educa.pdf");
	}

	public void Imprimir(){
		Application.OpenURL("http://gcg.inf.furb.br/visedu/life/visedutransito/mapa.pdf");
	}

	public void Sobre(){
		Application.OpenURL("http://www.inf.furb.br/gcg/visedu/life/visedutransito");	
	}

	public void Iniciar()
	{
		Application.LoadLevel ("JogoComJoystick");
		if (CarroConfig.Instancia.selecionouMarcadorMapa) {
			Application.LoadLevel ("ControlePorJoystick");
		} 
		else 
		{
			if (!string.IsNullOrEmpty (CarroConfig.Instancia.nomeCarroDoMarcadorSelecionado)) 
			{
				Application.LoadLevel ("ControlePorCarroVirtual");
				//Application.LoadLevel ("ControlePorCarroReal"); -- arrumar
			} 
			else 
			{
				GameObject Game = GameObject.FindGameObjectWithTag ("TextoMarcadorSelecao");
				if(Game != null){
					Game.GetComponent<Text> ().text = "Selecione um marcador para iniciar!";
					StartCoroutine (Agurdar (5));
				}
			}
		}
	}
}
