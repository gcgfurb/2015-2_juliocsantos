﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.UI;

public class SelecaoMarcadorScript : MonoBehaviour, ITrackableEventHandler {

	private TrackableBehaviour mTrackableBehaviour;

	// Use this for initialization
	void Start () 
	{
		this.mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			this.mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			OnTrackingFound();
		}
		else
		{
			OnTrackingLost();
		}
	}

	private void OnTrackingFound()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer> (true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider> (true);
		
		// Enable rendering:
		foreach (Renderer component in rendererComponents) 
		{
			component.enabled = true;
		}
		
		// Enable colliders:
		foreach (Collider component in colliderComponents) 
		{
			component.enabled = true;
		}

		Transform Carro = gameObject.transform.FindChild ("Carro Vermelho");
		if (Carro != null) 
		{
			CarroConfig.Instancia.selecionouMarcadorMapa = false;
			CarroConfig.Instancia.nomeCarroDoMarcadorSelecionado = "Carro Vermelho";
			CarroConfig.Instancia.tagCarroDoMarcadorSelecionado = "CarroVermelho";
			CarroConfig.Instancia.tagCarroAdversario = "CarroAmarelo";
		} 
		else 
		{
			Carro = gameObject.transform.FindChild ("Carro Amarelo");
			if(Carro != null)
			{
				CarroConfig.Instancia.selecionouMarcadorMapa = false;
				CarroConfig.Instancia.nomeCarroDoMarcadorSelecionado = "Carro Amarelo";
				CarroConfig.Instancia.tagCarroDoMarcadorSelecionado = "CarroAmarelo";
				CarroConfig.Instancia.tagCarroAdversario = "CarroVermelho";
			}
			else
			{
				CarroConfig.Instancia.selecionouMarcadorMapa = true;
			}
		}
		if (Carro == null) {
			GameObject.FindGameObjectWithTag ("BotaoVoltar").transform.FindChild ("Text").
				GetComponent<Text> ().text = "Selecionar carro com joystick";
		} else {

			GameObject.FindGameObjectWithTag ("BotaoVoltar").transform.FindChild ("Text").
			GetComponent<Text> ().text = string.Format ("Selecionar {0}", CarroConfig.Instancia.nomeCarroDoMarcadorSelecionado);
		}
	}
	
	private void OnTrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		
		// Disable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		
		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = false;
		}
	}
}
